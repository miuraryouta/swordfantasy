#include "MPBar.h"
#include "Engine/Image.h"
#include "Player.h"



//コンストラクタ
MPBar::MPBar(GameObject* parent)
	:GameObject(parent, "MPBar")
{
	for (int i = 0; i < 51; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
MPBar::~MPBar()
{
}

//初期化
void MPBar::Initialize()
{

	//画像データのロード
//	hPict_ = Image::Load("MPBar.png");
//	assert(hPict_ >= 0);

	for (int i = 0; i < 51; i++)
	{
		switch (i)
		{
			
		case 0:
			hPict_[i] = Image::Load("MPBar0.png");
			break;
		case 1:
			hPict_[i] = Image::Load("MPBar1.png");
			break;
		case 2:
			hPict_[i] = Image::Load("MPBar2.png");
			break;
		case 3:
			hPict_[i] = Image::Load("MPBar3.png");
			break;
		case 4:
			hPict_[i] = Image::Load("MPBar4.png");
			break;
		case 5:
			hPict_[i] = Image::Load("MPBar5.png");
			break;
		case 6:
			hPict_[i] = Image::Load("MPBar6.png");
			break;
		case 7:
			hPict_[i] = Image::Load("MPBar7.png");
			break;
		case 8:
			hPict_[i] = Image::Load("MPBar8.png");
			break;
		case 9:
			hPict_[i] = Image::Load("MPBar9.png");
			break;
		case 10:
			hPict_[i] = Image::Load("MPBar10.png");
			break;
		case 11:
			hPict_[i] = Image::Load("MPBar11.png");
			break;
		case 12:
			hPict_[i] = Image::Load("MPBar12.png");
			break;
		case 13:
			hPict_[i] = Image::Load("MPBar13.png");
			break;
		case 14:
			hPict_[i] = Image::Load("MPBar14.png");
			break;
		case 15:
			hPict_[i] = Image::Load("MPBar15.png");
			break;
		case 16:
			hPict_[i] = Image::Load("MPBar16.png");
			break;
		case 17:
			hPict_[i] = Image::Load("MPBar17.png");
			break;
		case 18:
			hPict_[i] = Image::Load("MPBar18.png");
			break;
		case 19:
			hPict_[i] = Image::Load("MPBar19.png");
			break;
		case 20:
			hPict_[i] = Image::Load("MPBar20.png");
			break;
		case 21:
			hPict_[i] = Image::Load("MPBar21.png");
			break;
		case 22:
			hPict_[i] = Image::Load("MPBar22.png");
			break;
		case 23:
			hPict_[i] = Image::Load("MPBar23.png");
			break;
		case 24:
			hPict_[i] = Image::Load("MPBar24.png");
			break;
		case 25:
			hPict_[i] = Image::Load("MPBar25.png");
			break;
		case 26:
			hPict_[i] = Image::Load("MPBar26.png");
			break;
		case 27:
			hPict_[i] = Image::Load("MPBar27.png");
			break;
		case 28:
			hPict_[i] = Image::Load("MPBar28.png");
			break;
		case 29:
			hPict_[i] = Image::Load("MPBar29.png");
			break;
		case 30:
			hPict_[i] = Image::Load("MPBar30.png");
			break;
		case 31:
			hPict_[i] = Image::Load("MPBar31.png");
			break;
		case 32:
			hPict_[i] = Image::Load("MPBar32.png");
			break;
		case 33:
			hPict_[i] = Image::Load("MPBar33.png");
			break;
		case 34:
			hPict_[i] = Image::Load("MPBar34.png");
			break;
		case 35:
			hPict_[i] = Image::Load("MPBar35.png");
			break;
		case 36:
			hPict_[i] = Image::Load("MPBar36.png");
			break;
		case 37:
			hPict_[i] = Image::Load("MPBar37.png");
			break;
		case 38:
			hPict_[i] = Image::Load("MPBar38.png");
			break;
		case 39:
			hPict_[i] = Image::Load("MPBar39.png");
			break;
		case 40:
			hPict_[i] = Image::Load("MPBar40.png");
			break;
		case 41:
			hPict_[i] = Image::Load("MPBar41.png");
			break;
		case 42:
			hPict_[i] = Image::Load("MPBar42.png");
			break;
		case 43:
			hPict_[i] = Image::Load("MPBar43.png");
			break;
		case 44:
			hPict_[i] = Image::Load("MPBar44.png");
			break;
		case 45:
			hPict_[i] = Image::Load("MPBar45.png");
			break;
		case 46:
			hPict_[i] = Image::Load("MPBar46.png");
			break;
		case 47:
			hPict_[i] = Image::Load("MPBar47.png");
			break;
		case 48:
			hPict_[i] = Image::Load("MPBar48.png");
			break;
		case 49:
			hPict_[i] = Image::Load("MPBar49.png");
			break;
		case 50:
			hPict_[i] = Image::Load("MPBar50.png");
			
		}
	}

	//

	//２番目に配置する
	transform_.position_.vecX -= 0.8f;
	transform_.position_.vecY -= 0.5f;

	transform_.scale_.vecX = 1.5f;
}

//更新
void MPBar::Update()
{
	Player* pPlayer_ = (Player*)FindObject("Player");

	MPflag = pPlayer_->MP_PL;


	//

	

}




//描画
void MPBar::Draw()
{
	Image::SetTransform(hPict_[MPflag], transform_);
	Image::Draw(hPict_[MPflag]);
}

//開放
void MPBar::Release()
{
}













