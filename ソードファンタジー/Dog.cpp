#include "Dog.h"
#include "Sword.h"
#include "FireMagic.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Player.h"
#include "Ground.h"




//コンストラクタ
Dog::Dog(GameObject * parent)
	:GameObject(parent, "Dog"), hModel_(-1)
{
}

//デストラクタ
Dog::~Dog()
{
}

//初期化
void Dog::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Dog.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = 25;

	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;
	transform_.scale_.vecZ = 2.0f;

	transform_.rotate_.vecY = 180;

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 1, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	time = 2;

	time2 = 1;

	time3 = 10;

	Model::SetAnimFrame(hModel_, 1, 150, 2);
}

//更新
void Dog::Update()
{
	Move();    //移動処理

	Special();     //移動処理以外
}

void Dog::Move()
{
	Player* pPlayer;
	pPlayer = (Player*)FindObject("Player");
	XMVECTOR direction = { 0.0f, 0.0f, 1.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	direction = XMVector3TransformCoord(direction, mat);

	XMVECTOR distance = pPlayer->GetPosition() - transform_.position_;



	distance = XMVector3Normalize(distance);
	direction = XMVector3Normalize(direction);

	float naiseki = XMVector3Dot(direction, distance).vecX;
	float angle = acos(naiseki);


	angle = XMConvertToDegrees(angle);



	XMVECTOR move_dg = { 0.0f, 0.0f, 0.3f, 0.0f };

	XMMATRIX mat_dg;
	mat_dg = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_dg = XMVector3TransformCoord(move_dg, mat_dg);



	if (VISION_DG >= angle)
	{
		//プレイヤーのいる方向に向かってゆく
		transform_.rotate_.vecY += 0;

		transform_.position_ += move_dg;
	}
	else if(VISION_DG > angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY += 2.0f;
	}
	else if (VISION_DG < angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY -= 2.0f;
	}
}

void Dog::Special()
{
	//HPが0以下になったとき
	if (HP_DG < 1)
	{
		KillMe(); //犬が死ぬ

		//シーンが変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR2);

	}


	XMVECTOR move_dg = { 0.0f, 0.0f, 0.3f, 0.0f };

	XMMATRIX mat_dg;
	mat_dg = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_dg = XMVector3TransformCoord(move_dg, mat_dg);

	if (flag == 1)
	{
		AT_DG = 0;

		transform_.position_ -= move_dg;

		//攻撃した後2秒間攻撃しない
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_DG)
			{


				switch (time)
				{

				case 2:TimerFlag_DG = 2; break;
				case 1:TimerFlag_DG = 1; break;
				case 0:TimerFlag_DG = 0;
				}
				time--;
				TimerFlag_DG--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_DG == 0)
		{
			AT_DG = 7;
			flag = 0;
			time = 2;
			TimerFlag_DG = 2;
		}
	}

	//特殊攻撃
	if (flag3 == 0)
	{
		Rand_DG = rand() % 10;
		flag3 = 1;
	}
	else if (flag3 == 1)
	{
		if (Rand_DG == 1)
		{
			if (flag4 == 0)
			{
				move_dg = move_dg * 5;
			}
			else if (flag4 == 1)
			{
				move_dg = move_dg / 5;

				flag4 = 0;
			}

			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_DG3)
				{
					switch (time3)
					{
					case 10:TimerFlag_DG3 = 10; break;
					case 9:TimerFlag_DG3 = 9; break;
					case 8:TimerFlag_DG3 = 8; break;
					case 7:TimerFlag_DG3 = 7; break;
					case 6:TimerFlag_DG3 = 6; break;
					case 5:TimerFlag_DG3 = 5; break;
					case 4:TimerFlag_DG3 = 4; break;
					case 3:TimerFlag_DG3 = 3; break;
					case 2:TimerFlag_DG3 = 2; break;
					case 1:TimerFlag_DG3 = 1; break;
					case 0:TimerFlag_DG3 = 0;
					}
					time3--;
					TimerFlag_DG3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_DG3 == 0)
			{
				Rand_DG = 0;
				flag3 = 0;
				time3 = 10;
				TimerFlag_DG3 = 10;
			}
		}
		else if (Rand_DG > 1)
		{
			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_DG3)
				{
					switch (time3)
					{
					case 10:TimerFlag_DG3 = 10; break;
					case 9:TimerFlag_DG3 = 9; break;
					case 8:TimerFlag_DG3 = 8; break;
					case 7:TimerFlag_DG3 = 7; break;
					case 6:TimerFlag_DG3 = 6; break;
					case 5:TimerFlag_DG3 = 5; break;
					case 4:TimerFlag_DG3 = 4; break;
					case 3:TimerFlag_DG3 = 3; break;
					case 2:TimerFlag_DG3 = 2; break;
					case 1:TimerFlag_DG3 = 1; break;
					case 0:TimerFlag_DG3 = 0;
					}
					time3--;
					TimerFlag_DG3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_DG3 == 0)
			{
				Rand_DG = 0;
				flag3 = 0;
				time3 = 10;
				TimerFlag_DG3 = 10;
			}
		}
	}
}

//描画
void Dog::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Dog::Release()
{
}

//何かに当たった
void Dog::OnCollision(GameObject * pTarget)
{
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");
	Player* pPlayer_ = (Player*)FindObject("Player");
	Ground* pGround_ = (Ground*)FindObject("Ground");

	//剣に当たったとき
	if (pTarget->GetObjectName() == "Sword")
	{
		//Eキーを押した
		if (Input::IsKeyDown(DIK_E))
		{
			if (pPlayer_->flag7 != 5)
			{


				pPlayer_->MP_PL += 5;

				if (pPlayer_->flag1 == 0)
				{
					pPlayer_->SP_PL += 5;
				}

				//HPが減る
				HP_DG = HP_DG - pSword_->AT_S;

				pPlayer_->flag7 += 1;
				
			}
		}



	}

	//魔法に当たったとき
	if (pTarget->GetObjectName() == "FireMagic")
	{
		HP_DG = HP_DG - pFireMagic_->AT_FM;
		pTarget->KillMe(); //魔法が消える
	}

	XMVECTOR move_dg = { 0.0f, 0.0f, 0.3f, 0.0f };

	XMMATRIX mat_dg;
	mat_dg = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_dg = XMVector3TransformCoord(move_dg, mat_dg);

	//プレイヤーにあたったとき
	if (pTarget->GetObjectName() == "Player")
	{
		transform_.position_ -= move_dg;

		if (flag2 == 0)
		{
			flag2 = 1;
		}
		else if (flag2 == 1)
		{
			AT_DG = 0;

			

			//攻撃前1秒間攻撃しない
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time2 == TimerFlag_DG2)
				{


					switch (time2)
					{
					case 1:TimerFlag_DG2 = 1; break;
					case 0:TimerFlag_DG2 = 0;
					}
					time2--;
					TimerFlag_DG2--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_DG2 == 0)
			{
				AT_DG = 7;
				flag2 = 0;
				time2 = 1;
				TimerFlag_DG2 = 1;
			}
		}

		if (flag == 0)
		{
			//フラグが0のときプレイヤーに攻撃できる
			pPlayer_->HP_PL = pPlayer_->HP_PL - AT_DG;
			flag = 1;
		}

		if (flag4 == 0)
		{
			if (Rand_DG == 1)
			{
				flag4 = 1;
			}
		}


	}

	//に当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		transform_.position_ -= move_dg;
	}
}

void Dog::CountDown()
{
	time -= 1;

	time2 -= 1;

	time3 -= 1;
}


