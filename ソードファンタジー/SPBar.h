#pragma once
#include "Engine/GameObject.h"

//SPバーを管理するクラス
class SPBar : public GameObject
{
	int hPict_[101];    //画像番号

	int hSound_;    //サウンド番号

	int SPflag = 0;

public:



	//コンストラクタ
	SPBar(GameObject* parent);

	//デストラクタ
	~SPBar();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};