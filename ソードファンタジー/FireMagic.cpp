#include "FireMagic.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
FireMagic::FireMagic(GameObject * parent)
	:GameObject(parent, "FireMagic"), hModel_(-1), move_({ 0, 0, 0.5, 0 })
{
}

//デストラクタ
FireMagic::~FireMagic()
{
}

//初期化
void FireMagic::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("fire.fbx");
	assert(hModel_ >= 0);

	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0.3, 0, 0), 0.3f);
	AddCollider(collision);
}

//更新
void FireMagic::Update()
{
	//弾が奥に移動する
	transform_.position_ += move_;

	//弾が奥まで行ったら消える
	if (transform_.position_.vecZ >= 200) {
		KillMe();
	}
	else if (transform_.position_.vecZ <= -200)
	{
		KillMe();
	}
	else if (transform_.position_.vecX >= 200)
	{
		KillMe();
	}
	else if (transform_.position_.vecX <= -200)
	{
		KillMe();
	}
}

//描画
void FireMagic::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void FireMagic::Release()
{
}

//弾を発射
void FireMagic::Shot(XMVECTOR position, XMVECTOR direction)
{
	transform_.position_ = position;
	//正規化してスカラー倍する
	move_ = XMVector3Normalize(direction) * SPEED;


}