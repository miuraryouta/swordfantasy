#include "sceneManager.h"
#include "../PlayScene1.h"
#include "../PlayScene2.h"
#include "../PlayScene3.h"
#include "../PlayScene4.h"
#include "../PlayScene5.h"
#include "../PlayScene6.h"
#include "../TitleScene.h"
#include "../TestScene.h"
#include "../GameClearScene1.h"
#include "../GameClearScene2.h"
#include "../GameClearScene3.h"
#include "../GameClearScene4.h"
#include "../GameClearScene5.h"
#include "../GameClearScene6.h"
#include "../GameOverScene1.h"
#include "../GameOverScene2.h"
#include "../GameOverScene3.h"
#include "../GameOverScene4.h"
#include "../GameOverScene5.h"
#include "../GameOverScene6.h"
#include "../SplashScene.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"


//コンストラクタ
SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager")
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_SPLASH;
	nextSceneID_ = currentSceneID_;
	Instantiate<SplashScene>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Audio::Release();
		Model::AllRelease();
		Image::AllRelease();

		//次のシーンを作成
		switch (nextSceneID_)
		{
		case SCENE_ID_TEST: Instantiate<TestScene>(this); break;
		case SCENE_ID_PLAY1: Instantiate<PlayScene1>(this); break;
		case SCENE_ID_PLAY2: Instantiate<PlayScene2>(this); break;
		case SCENE_ID_PLAY3: Instantiate<PlayScene3>(this); break;
		case SCENE_ID_PLAY4: Instantiate<PlayScene4>(this); break;
		case SCENE_ID_PLAY5: Instantiate<PlayScene5>(this); break;
		case SCENE_ID_PLAY6: Instantiate<PlayScene6>(this); break;
		case SCENE_ID_TITLE: Instantiate<TitleScene>(this); break;
		case SCENE_ID_GAMECLEAR1: Instantiate<GameClearScene1>(this); break;
		case SCENE_ID_GAMECLEAR2: Instantiate<GameClearScene2>(this); break;
		case SCENE_ID_GAMECLEAR3: Instantiate<GameClearScene3>(this); break;
		case SCENE_ID_GAMECLEAR4: Instantiate<GameClearScene4>(this); break;
		case SCENE_ID_GAMECLEAR5: Instantiate<GameClearScene5>(this); break;
		case SCENE_ID_GAMECLEAR6: Instantiate<GameClearScene6>(this); break;
		case SCENE_ID_GAMEOVER1: Instantiate<GameOverScene1>(this); break;
		case SCENE_ID_GAMEOVER2: Instantiate<GameOverScene2>(this); break;
		case SCENE_ID_GAMEOVER3: Instantiate<GameOverScene3>(this); break;
		case SCENE_ID_GAMEOVER4: Instantiate<GameOverScene4>(this); break;
		case SCENE_ID_GAMEOVER5: Instantiate<GameOverScene5>(this); break;
		case SCENE_ID_GAMEOVER6: Instantiate<GameOverScene6>(this); break;
		case SCENE_ID_SPLASH: Instantiate<SplashScene>(this); break;
		}
		Audio::Initialize();
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}