#pragma once
#include "global.h"
#include "GameObject.h"

//ゲームに登場するシーン
enum SCENE_ID
{
	SCENE_ID_TEST = 0,
	SCENE_ID_PLAY1,
	SCENE_ID_PLAY2,
	SCENE_ID_PLAY3,
	SCENE_ID_PLAY4,
	SCENE_ID_PLAY5,
	SCENE_ID_PLAY6,
	SCENE_ID_TITLE,
	SCENE_ID_GAMECLEAR1,
	SCENE_ID_GAMECLEAR2,
	SCENE_ID_GAMECLEAR3,
	SCENE_ID_GAMECLEAR4,
	SCENE_ID_GAMECLEAR5,
	SCENE_ID_GAMECLEAR6,
	SCENE_ID_GAMEOVER1,
	SCENE_ID_GAMEOVER2,
	SCENE_ID_GAMEOVER3,
	SCENE_ID_GAMEOVER4,
	SCENE_ID_GAMEOVER5,
	SCENE_ID_GAMEOVER6,
	SCENE_ID_SPLASH,
};

//-----------------------------------------------------------
//シーン切り替えを担当するオブジェクト
//-----------------------------------------------------------
class SceneManager : public GameObject
{
public:

	//コンストラクタ
	//引数：parent	親オブジェクト（基本的にゲームマネージャー）
	SceneManager(GameObject* parent);

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//引数：next	次のシーンのID
	void ChangeScene(SCENE_ID next);

private:
	SCENE_ID currentSceneID_;	//現在のシーン
	SCENE_ID nextSceneID_;		//次のシーン

};