#pragma once
#include "Engine/GameObject.h"

//ゴブリンを管理するクラス
class Goblin : public GameObject
{
	int hModel_;    //モデル番号
	
	const int VISION_GB = 1;  //視野

	int time;

	int time2;

	int time3;

	char TimerFlag_GB = 3;

	char TimerFlag_GB2 = 1;

	char TimerFlag_GB3 = 10;

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//移動処理以外
	//引数：なし
	//戻り値：なし
	void Special();

public:
	//コンストラクタ
	Goblin(GameObject* parent);

	//デストラクタ
	~Goblin();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//乱数
	int Rand_GB;

	//ステータス
	int HP_GB = 75; //体力

	int AT_GB = 14; //攻撃力

	//フラグ
	int flag = 0;

	int flag2 = 0;

	int flag3 = 0;

	

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();
};