#include "Dragon.h"
#include "Sword.h"
#include "Player.h"
#include "FireMagic.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "FireAttackRange.h"
#include "FireAttack_DR.h"
#include "Ground.h"






//コンストラクタ
Dragon::Dragon(GameObject* parent)
	:GameObject(parent, "Dragon"), hModel_(-1)
{
}

//デストラクタ
Dragon::~Dragon()
{
}

//初期化
void Dragon::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Dragon.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = 25;

	transform_.scale_.vecX = 5.0f;
	transform_.scale_.vecY = 5.0f;
	transform_.scale_.vecZ = 5.0f;

	transform_.rotate_.vecY = 180;

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 1.5, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	time = 5;

	time2 = 1;

	Instantiate<FireAttack_DR>(this);

	Model::SetAnimFrame(hModel_, 1, 150, 2);
	//

}

//更新
void Dragon::Update()
{
	Move();       //移動処理

	Special();      //移動処理以外


}

void Dragon::Move()
{
	Player* pPlayer_ = (Player*)FindObject("Player");

	XMVECTOR direction = { 0.0f, 0.0f, 1.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	direction = XMVector3TransformCoord(direction, mat);

	XMVECTOR distance = pPlayer_->GetPosition() - transform_.position_;



	distance = XMVector3Normalize(distance);
	direction = XMVector3Normalize(direction);

	float naiseki = XMVector3Dot(direction, distance).vecX;
	float angle = acos(naiseki);


	angle = XMConvertToDegrees(angle);



	XMVECTOR move_dr = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_dr;
	mat_dr = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_dr = XMVector3TransformCoord(move_dr, mat_dr);

	if (VISION_DR >= angle)
	{
		//プレイヤーのいる方向に向かってゆく
		transform_.position_ += move_dr;
	}
	else if(VISION_DR > angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY += 2.0f;
	}
	else if (VISION_DR < angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY -= 2.0f;
	}
}

void Dragon::Special()
{
	if (HP_DR < 1)
	{
		KillMe();

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR6);
	}


	XMVECTOR move_dr = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_dr;
	mat_dr = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_dr = XMVector3TransformCoord(move_dr, mat_dr);

	if (flag == 1)
	{
		AT_DR = 0;

		transform_.position_ -= move_dr;

		//攻撃した後5秒間攻撃しない
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_DR)
			{


				switch (time)
				{
				case 5:TimerFlag_DR = 5; break;
				case 4:TimerFlag_DR = 4; break;
				case 3:TimerFlag_DR = 3; break;
				case 2:TimerFlag_DR = 2; break;
				case 1:TimerFlag_DR = 1; break;
				case 0:TimerFlag_DR = 0;
				}
				time--;
				TimerFlag_DR--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_DR == 0)
		{
			AT_DR = 25;
			flag = 0;
			time = 5;
			TimerFlag_DR = 5;
		}
	}

	FireAttackRange* pFAR_ = (FireAttackRange*)FindObject("FireAttackRange");
	FireAttack_DR* pFireAttack_DR = (FireAttack_DR*)FindObject("FireAttack_DR");

	//特殊攻撃
	if (pFAR_->flag_FAR == 1)
	{
		//炎で攻撃
		FireAttack_DR* pFireAttack_DR = Instantiate<FireAttack_DR>(GetParent());
		pFireAttack_DR->SetPosition(transform_.position_);

		pFAR_->flag_FAR = 0;

		pFAR_->Rand_FAR = 0;

		pFireAttack_DR->flag = 1;

	}
}

//描画
void Dragon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Dragon::Release()
{
}

void Dragon::OnCollision(GameObject * pTarget)
{
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");
	Player* pPlayer_ = (Player*)FindObject("Player");
	Ground* pGround_ = (Ground*)FindObject("Ground");


	//剣に当たったとき
	if (pTarget->GetObjectName() == "Sword")
	{
		//Eキーを押した
		if (Input::IsKeyDown(DIK_E))
		{
			if (pPlayer_->flag7 != 5)
			{

				pPlayer_->MP_PL += 5;

				if (pPlayer_->flag1 == 0)
				{
					pPlayer_->SP_PL += 5;
				}

				//HPが減る
				HP_DR = HP_DR - pSword_->AT_S;

				
				pPlayer_->flag7 += 1;
				
			}
		}



	}

	//魔法に当たったとき
	if (pTarget->GetObjectName() == "FireMagic")
	{
		HP_DR = HP_DR - pFireMagic_->AT_FM;
		pTarget->KillMe(); //魔法が消える
	}

	XMVECTOR move_dr = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_dr;
	mat_dr = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_dr = XMVector3TransformCoord(move_dr, mat_dr);

	//プレイヤーに当たったとき
	if (pTarget->GetObjectName() == "Player")
	{
		transform_.position_ -= move_dr;  //止まる

		if (flag2 == 0)
		{
			flag2 = 1;
		}
		else if (flag2 == 1)
		{
			AT_DR = 0;

			

			//攻撃前1秒間攻撃しない
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time2 == TimerFlag_DR2)
				{


					switch (time2)
					{
					case 1:TimerFlag_DR2 = 1; break;
					case 0:TimerFlag_DR2 = 0;
					}
					time2--;
					TimerFlag_DR2--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_DR2 == 0)
			{
				AT_DR = 25;
				flag2 = 0;
				time2 = 1;
				TimerFlag_DR2 = 1;
			}
		}

		if (flag == 0)
		{
			pPlayer_->HP_PL = pPlayer_->HP_PL - AT_DR;
			flag = 1;
		}


	}

	//に当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		transform_.position_ -= move_dr;
	}
}
//

void Dragon::CountDown()
{
	time -= 1;

	time2 -= 1;
}
