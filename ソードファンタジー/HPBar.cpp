#include "HPBar.h"
#include "Engine/Image.h"
#include "Player.h"
#include "Slime.h"
#include "Dog.h"
#include "Goblin.h"
#include "bird.h"
#include "Minotaur.h"
#include "Dragon.h"
#include "Engine/Audio.h"



//コンストラクタ
HPBar::HPBar(GameObject* parent)
	:GameObject(parent, "HPBar"), hSound_(-1)
{
	for (int i = 0; i < 101; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
HPBar::~HPBar()
{
}

//初期化
void HPBar::Initialize()
{

	//画像データのロード
//	hPict_ = Image::Load("HPBar.png");
//	assert(hPict_ >= 0);

	for (int i = 0; i < 101; i++)
	{
		switch (i)
		{
		case 0:
			hPict_[i] = Image::Load("HPBar0.png");
			break;
		case 1:
			hPict_[i] = Image::Load("HPBar1.png");
			break;
		case 2:
			hPict_[i] = Image::Load("HPBar2.png");
			break;
		case 3:
			hPict_[i] = Image::Load("HPBar3.png");
			break;
		case 4:
			hPict_[i] = Image::Load("HPBar4.png");
			break;
		case 5:
			hPict_[i] = Image::Load("HPBar5.png");
			break;
		case 6:
			hPict_[i] = Image::Load("HPBar6.png");
			break;
		case 7:
			hPict_[i] = Image::Load("HPBar7.png");
			break;
		case 8:
			hPict_[i] = Image::Load("HPBar8.png");
			break;
		case 9:
			hPict_[i] = Image::Load("HPBar9.png");
			break;
		case 10:
			hPict_[i] = Image::Load("HPBar10.png");
			break;
		case 11:
			hPict_[i] = Image::Load("HPBar11.png");
			break;
		case 12:
			hPict_[i] = Image::Load("HPBar12.png");
			break;
		case 13:
			hPict_[i] = Image::Load("HPBar13.png");
			break;
		case 14:
			hPict_[i] = Image::Load("HPBar14.png");
			break;
		case 15:
			hPict_[i] = Image::Load("HPBar15.png");
			break;
		case 16:
			hPict_[i] = Image::Load("HPBar16.png");
			break;
		case 17:
			hPict_[i] = Image::Load("HPBar17.png");
			break;
		case 18:
			hPict_[i] = Image::Load("HPBar18.png");
			break;
		case 19:
			hPict_[i] = Image::Load("HPBar19.png");
			break;
		case 20:
			hPict_[i] = Image::Load("HPBar20.png");
			break;
		case 21:
			hPict_[i] = Image::Load("HPBar21.png");
			break;
		case 22:
			hPict_[i] = Image::Load("HPBar22.png");
			break;
		case 23:
			hPict_[i] = Image::Load("HPBar23.png");
			break;
		case 24:
			hPict_[i] = Image::Load("HPBar24.png");
			break;
		case 25:
			hPict_[i] = Image::Load("HPBar25.png");
			break;
		case 26:
			hPict_[i] = Image::Load("HPBar26.png");
			break;
		case 27:
			hPict_[i] = Image::Load("HPBar27.png");
			break;
		case 28:
			hPict_[i] = Image::Load("HPBar28.png");
			break;
		case 29:
			hPict_[i] = Image::Load("HPBar29.png");
			break;
		case 30:
			hPict_[i] = Image::Load("HPBar30.png");
			break;
		case 31:
			hPict_[i] = Image::Load("HPBar31.png");
			break;
		case 32:
			hPict_[i] = Image::Load("HPBar32.png");
			break;
		case 33:
			hPict_[i] = Image::Load("HPBar33.png");
			break;
		case 34:
			hPict_[i] = Image::Load("HPBar34.png");
			break;
		case 35:
			hPict_[i] = Image::Load("HPBar35.png");
			break;
		case 36:
			hPict_[i] = Image::Load("HPBar36.png");
			break;
		case 37:
			hPict_[i] = Image::Load("HPBar37.png");
			break;
		case 38:
			hPict_[i] = Image::Load("HPBar38.png");
			break;
		case 39:
			hPict_[i] = Image::Load("HPBar39.png");
			break;
		case 40:
			hPict_[i] = Image::Load("HPBar40.png");
			break;
		case 41:
			hPict_[i] = Image::Load("HPBar41.png");
			break;
		case 42:
			hPict_[i] = Image::Load("HPBar42.png");
			break;
		case 43:
			hPict_[i] = Image::Load("HPBar43.png");
			break;
		case 44:
			hPict_[i] = Image::Load("HPBar44.png");
			break;
		case 45:
			hPict_[i] = Image::Load("HPBar45.png");
			break;
		case 46:
			hPict_[i] = Image::Load("HPBar46.png");
			break;
		case 47:
			hPict_[i] = Image::Load("HPBar47.png");
			break;
		case 48:
			hPict_[i] = Image::Load("HPBar48.png");
			break;
		case 49:
			hPict_[i] = Image::Load("HPBar49.png");
			break;
		case 50:
			hPict_[i] = Image::Load("HPBar50.png");
			break;
		case 51:
			hPict_[i] = Image::Load("HPBar51.png");
			break;
		case 52:
			hPict_[i] = Image::Load("HPBar52.png");
			break;
		case 53:
			hPict_[i] = Image::Load("HPBar53.png");
			break;
		case 54:
			hPict_[i] = Image::Load("HPBar54.png");
			break;
		case 55:
			hPict_[i] = Image::Load("HPBar55.png");
			break;
		case 56:
			hPict_[i] = Image::Load("HPBar56.png");
			break;
		case 57:
			hPict_[i] = Image::Load("HPBar57.png");
			break;
		case 58:
			hPict_[i] = Image::Load("HPBar58.png");
			break;
		case 59:
			hPict_[i] = Image::Load("HPBar59.png");
			break;
		case 60:
			hPict_[i] = Image::Load("HPBar60.png");
			break;
		case 61:
			hPict_[i] = Image::Load("HPBar61.png");
			break;
		case 62:
			hPict_[i] = Image::Load("HPBar62.png");
			break;
		case 63:
			hPict_[i] = Image::Load("HPBar63.png");
			break;
		case 64:
			hPict_[i] = Image::Load("HPBar64.png");
			break;
		case 65:
			hPict_[i] = Image::Load("HPBar65.png");
			break;
		case 66:
			hPict_[i] = Image::Load("HPBar66.png");
			break;
		case 67:
			hPict_[i] = Image::Load("HPBar67.png");
			break;
		case 68:
			hPict_[i] = Image::Load("HPBar68.png");
			break;
		case 69:
			hPict_[i] = Image::Load("HPBar69.png");
			break;
		case 70:
			hPict_[i] = Image::Load("HPBar70.png");
			break;
		case 71:
			hPict_[i] = Image::Load("HPBar71.png");
			break;
		case 72:
			hPict_[i] = Image::Load("HPBar72.png");
			break;
		case 73:
			hPict_[i] = Image::Load("HPBar73.png");
			break;
		case 74:
			hPict_[i] = Image::Load("HPBar74.png");
			break;
		case 75:
			hPict_[i] = Image::Load("HPBar75.png");
			break;
		case 76:
			hPict_[i] = Image::Load("HPBar76.png");
			break;
		case 77:
			hPict_[i] = Image::Load("HPBar77.png");
			break;
		case 78:
			hPict_[i] = Image::Load("HPBar78.png");
			break;
		case 79:
			hPict_[i] = Image::Load("HPBar79.png");
			break;
		case 80:
			hPict_[i] = Image::Load("HPBar80.png");
			break;
		case 81:
			hPict_[i] = Image::Load("HPBar81.png");
			break;
		case 82:
			hPict_[i] = Image::Load("HPBar82.png");
			break;
		case 83:
			hPict_[i] = Image::Load("HPBar83.png");
			break;
		case 84:
			hPict_[i] = Image::Load("HPBar84.png");
			break;
		case 85:
			hPict_[i] = Image::Load("HPBar85.png");
			break;
		case 86:
			hPict_[i] = Image::Load("HPBar86.png");
			break;
		case 87:
			hPict_[i] = Image::Load("HPBar87.png");
			break;
		case 88:
			hPict_[i] = Image::Load("HPBar88.png");
			break;
		case 89:
			hPict_[i] = Image::Load("HPBar89.png");
			break;
		case 90:
			hPict_[i] = Image::Load("HPBar90.png");
			break;
		case 91:
			hPict_[i] = Image::Load("HPBar91.png");
			break;
		case 92:
			hPict_[i] = Image::Load("HPBar92.png");
			break;
		case 93:
			hPict_[i] = Image::Load("HPBar93.png");
			break;
		case 94:
			hPict_[i] = Image::Load("HPBar94.png");
			break;
		case 95:
			hPict_[i] = Image::Load("HPBar95.png");
			break;
		case 96:
			hPict_[i] = Image::Load("HPBar96.png");
			break;
		case 97:
			hPict_[i] = Image::Load("HPBar97.png");
			break;
		case 98:
			hPict_[i] = Image::Load("HPBar98.png");
			break;
		case 99:
			hPict_[i] = Image::Load("HPBar99.png");
			break;
		case 100:
			hPict_[i] = Image::Load("HPBar100.png");
		}
	}

	//１番上に配置する
	transform_.position_.vecX -= 0.8f;
	transform_.position_.vecY -= 0.7f;

	transform_.scale_.vecX = 1.5f;

	//サウンドデータのロード
//	hSound_ = Audio::Load("");
//	assert(hSound_ >= 0);

}

//更新
void HPBar::Update()
{
	Player* pPlayer_ = (Player*)FindObject("Player");
	Slime* pSlime_ = (Slime*)FindObject("Slime");
	Dog* pDog_ = (Dog*)FindObject("Dog");
	Goblin* pGoblin_ = (Goblin*)FindObject("Goblin");
	bird* pbird_ = (bird*)FindObject("bird");
	Minotaur* pMinotaur_ = (Minotaur*)FindObject("Minotaur");
	Dragon* pDragon_ = (Dragon*)FindObject("Dragon");

	HPflag = pPlayer_->HP_PL;

	//HPが減ったら攻撃が当たった音を鳴らす
	/*
	if (pSlime_->flag_SL == 1)
	{
		Audio::Play(hSound_);
	}
	else if (pDog_->flag == 1)
	{
		Audio::Play(hSound_);
	}
	else if (pGoblin_->flag == 1)
	{
		Audio::Play(hSound_);
	}
	else if (pbird_->flag == 1)
	{
		Audio::Play(hSound_);
	}
	else if (pMinotaur_->flag == 1)
	{
		Audio::Play(hSound_);
	}
	else if (pDragon_->flag == 1)
	{
		Audio::Play(hSound_);
	}
	*/


	if (HPflag == 0)
	{
		//HPが０になったら画像を消す
		KillMe();
	}

}




//描画
void HPBar::Draw()
{
	Image::SetTransform(hPict_[HPflag], transform_);
	Image::Draw(hPict_[HPflag]);
}

//開放
void HPBar::Release()
{
}













