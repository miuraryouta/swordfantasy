#include "SplashScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/Image.h"


//コンストラクタ
SplashScene::SplashScene(GameObject * parent)
	: GameObject(parent, "SplashScene"), hPict_(-1)
{
}

//初期化
void SplashScene::Initialize()
{
	time = 2;

	//画像データのロード
	hPict_ = Image::Load("Splash.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecY = 3.0f;
	transform_.scale_.vecX = 3.0f;
}

//更新
void SplashScene::Update()
{

	//2秒後にタイトルシーンに行く
	timeBeginPeriod(1);
	static DWORD lastFpsResetTime = timeGetTime();
	DWORD nowTime = timeGetTime();
	if (nowTime - lastFpsResetTime > 1000)
	{
		while (time == TimerFlag_)
		{


			switch (time)
			{
			case 2:TimerFlag_ = 2; break;
			case 1:TimerFlag_ = 1; break;
			case 0:TimerFlag_ = 0;
			}
			time--;
			TimerFlag_--;
			break;
		}
		lastFpsResetTime = nowTime;
	}
	if (TimerFlag_ == 0)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SplashScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void SplashScene::Release()
{
}

void SplashScene::CountDown()
{
	time -= 1;
}
