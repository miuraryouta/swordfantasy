#include "Ground.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"

//コンストラクタ
Ground::Ground(GameObject * parent)
	:GameObject(parent, "Ground"), hModel_(-1)
{
}

//デストラクタ
Ground::~Ground()
{
}

//初期化
void Ground::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Ground.fbx");
	assert(hModel_ >= 0);

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0, 0, 0, 0));
	AddCollider(collision);
}

//更新
void Ground::Update()
{
}

//描画
void Ground::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Ground::Release()
{
}

void Ground::OnCollision(GameObject* pTarget)
{
}
