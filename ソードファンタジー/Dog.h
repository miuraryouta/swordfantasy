#pragma once
#include "Engine/GameObject.h"

//犬を管理するクラス
class Dog : public GameObject
{
	int hModel_;    //モデル番号
	
	const int VISION_DG = 1;  //視野

	int time;

	int time2;

	int time3;

	char TimerFlag_DG = 2;

	char TimerFlag_DG2 = 1;

	char TimerFlag_DG3 = 10;

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//移動処理以外
	//引数：なし
	//戻り値：なし
	void Special();

public:



	//コンストラクタ
	Dog(GameObject* parent);

	//デストラクタ
	~Dog();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
    //引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();


	//乱数
	int Rand_DG;

	

	//ステータス
	int HP_DG = 50; //体力

	int AT_DG = 7; //攻撃力

	//フラグ
	int flag = 0;

	int flag2 = 0;

	int flag3 = 0;

	int flag4 = 0;
};