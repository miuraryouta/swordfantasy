#pragma once
#include "Engine/GameObject.h"

//鳥を管理するクラス
class bird : public GameObject
{
	int hModel_;    //モデル番号

	const int VISION_BI = 1;  //視線

	int time;

	int time2;

	int time3;

	char TimerFlag_BI = 4;

	char TimerFlag_BI2 = 1;

	char TimerFlag_BI3 = 10;

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//移動処理以外
	//引数：なし
	//戻り値：なし
	void Special();

	
public:
	//コンストラクタ
	bird(GameObject* parent);

	//デストラクタ
	~bird();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ステータス
	int HP_BI = 100;  //体力

	int AT_BI = 15;  //攻撃力

	//乱数
	int Rand_BI;

	//フラグ
	int flag = 0;

	int flag2 = 0;

	int flag3 = 0;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void CountDown();
};