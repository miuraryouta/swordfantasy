#include "FireAttackRange.h"
#include "Engine/BoxCollider.h"
#include "Player.h"

//コンストラクタ
FireAttackRange::FireAttackRange(GameObject* parent)
    :GameObject(parent, "FireAttackRange")
{
}

//デストラクタ
FireAttackRange::~FireAttackRange()
{
}

//初期化
void FireAttackRange::Initialize()
{
    BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(1, 1, 1, 0));
    AddCollider(collision);

    time = 10;
}

//更新
void FireAttackRange::Update()
{
    if (flag == 0)
    {
        Rand_FAR = rand() % 10;
        flag = 1;
    }
    else if (flag == 1)
    {
		//10秒間待つ
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_)
			{
				switch (time)
				{
				case 10:TimerFlag_ = 10; break;
				case 9:TimerFlag_ = 9; break;
				case 8:TimerFlag_ = 8; break;
				case 7:TimerFlag_ = 7; break;
				case 6:TimerFlag_ = 6; break;
				case 5:TimerFlag_ = 5; break;
				case 4:TimerFlag_ = 4; break;
				case 3:TimerFlag_ = 3; break;
				case 2:TimerFlag_ = 2; break;
				case 1:TimerFlag_ = 1; break;
				case 0:TimerFlag_ = 0;
				}
				time--;
				TimerFlag_--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_ == 0)
		{
			Rand_FAR = 0;
			flag = 0;
			time = 10;
			TimerFlag_ = 10;
		}
    }

}

//描画
void FireAttackRange::Draw()
{
}

//開放
void FireAttackRange::Release()
{
}

//何かに当たった
void FireAttackRange::OnCollision(GameObject* pTarget)
{
    Player* pPlayer_ = (Player*)FindObject("Player");

    //プレイヤーに当たったとき
    if (pTarget->GetObjectName() == "Player")
    {
        if (Rand_FAR == 1)
        {
            flag_FAR = 1;
        }
        else if(Rand_FAR > 1)
        {
            flag_FAR = 0;
        }
    }

}

void FireAttackRange::CountDown()
{
    time -= 1;
}
