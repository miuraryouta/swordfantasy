#include "STBar.h"
#include "Engine/Image.h"
#include "Player.h"



//コンストラクタ
STBar::STBar(GameObject* parent)
	:GameObject(parent, "STBar")
{
	for (int i = 0; i < 101; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
STBar::~STBar()
{
}

//初期化
void STBar::Initialize()
{

	//画像データのロード
//	hPict_ = Image::Load("STBar.png");
//	assert(hPict_ >= 0);

	for (int i = 0; i < 101; i++)
	{
		switch (i)
		{
			
		case 0:
			hPict_[i] = Image::Load("STBar0.png");
			break;
		case 1:
			hPict_[i] = Image::Load("STBar1.png");
			break;
		case 2:
			hPict_[i] = Image::Load("STBar2.png");
			break;
		case 3:
			hPict_[i] = Image::Load("STBar3.png");
			break;
		case 4:
			hPict_[i] = Image::Load("STBar4.png");
			break;
		case 5:
			hPict_[i] = Image::Load("STBar5.png");
			break;
		case 6:
			hPict_[i] = Image::Load("STBar6.png");
			break;
		case 7:
			hPict_[i] = Image::Load("STBar7.png");
			break;
		case 8:
			hPict_[i] = Image::Load("STBar8.png");
			break;
		case 9:
			hPict_[i] = Image::Load("STBar9.png");
			break;
		case 10:
			hPict_[i] = Image::Load("STBar10.png");
			break;
		case 11:
			hPict_[i] = Image::Load("STBar11.png");
			break;
		case 12:
			hPict_[i] = Image::Load("STBar12.png");
			break;
		case 13:
			hPict_[i] = Image::Load("STBar13.png");
			break;
		case 14:
			hPict_[i] = Image::Load("STBar14.png");
			break;
		case 15:
			hPict_[i] = Image::Load("STBar15.png");
			break;
		case 16:
			hPict_[i] = Image::Load("STBar16.png");
			break;
		case 17:
			hPict_[i] = Image::Load("STBar17.png");
			break;
		case 18:
			hPict_[i] = Image::Load("STBar18.png");
			break;
		case 19:
			hPict_[i] = Image::Load("STBar19.png");
			break;
		case 20:
			hPict_[i] = Image::Load("STBar20.png");
			break;
		case 21:
			hPict_[i] = Image::Load("STBar21.png");
			break;
		case 22:
			hPict_[i] = Image::Load("STBar22.png");
			break;
		case 23:
			hPict_[i] = Image::Load("STBar23.png");
			break;
		case 24:
			hPict_[i] = Image::Load("STBar24.png");
			break;
		case 25:
			hPict_[i] = Image::Load("STBar25.png");
			break;
		case 26:
			hPict_[i] = Image::Load("STBar26.png");
			break;
		case 27:
			hPict_[i] = Image::Load("STBar27.png");
			break;
		case 28:
			hPict_[i] = Image::Load("STBar28.png");
			break;
		case 29:
			hPict_[i] = Image::Load("STBar29.png");
			break;
		case 30:
			hPict_[i] = Image::Load("STBar30.png");
			break;
		case 31:
			hPict_[i] = Image::Load("STBar31.png");
			break;
		case 32:
			hPict_[i] = Image::Load("STBar32.png");
			break;
		case 33:
			hPict_[i] = Image::Load("STBar33.png");
			break;
		case 34:
			hPict_[i] = Image::Load("STBar34.png");
			break;
		case 35:
			hPict_[i] = Image::Load("STBar35.png");
			break;
		case 36:
			hPict_[i] = Image::Load("STBar36.png");
			break;
		case 37:
			hPict_[i] = Image::Load("STBar37.png");
			break;
		case 38:
			hPict_[i] = Image::Load("STBar38.png");
			break;
		case 39:
			hPict_[i] = Image::Load("STBar39.png");
			break;
		case 40:
			hPict_[i] = Image::Load("STBar40.png");
			break;
		case 41:
			hPict_[i] = Image::Load("STBar41.png");
			break;
		case 42:
			hPict_[i] = Image::Load("STBar42.png");
			break;
		case 43:
			hPict_[i] = Image::Load("STBar43.png");
			break;
		case 44:
			hPict_[i] = Image::Load("STBar44.png");
			break;
		case 45:
			hPict_[i] = Image::Load("STBar45.png");
			break;
		case 46:
			hPict_[i] = Image::Load("STBar46.png");
			break;
		case 47:
			hPict_[i] = Image::Load("STBar47.png");
			break;
		case 48:
			hPict_[i] = Image::Load("STBar48.png");
			break;
		case 49:
			hPict_[i] = Image::Load("STBar49.png");
			break;
		case 50:
			hPict_[i] = Image::Load("STBar50.png");
			break;
		case 51:
			hPict_[i] = Image::Load("STBar51.png");
			break;
		case 52:
			hPict_[i] = Image::Load("STBar52.png");
			break;
		case 53:
			hPict_[i] = Image::Load("STBar53.png");
			break;
		case 54:
			hPict_[i] = Image::Load("STBar54.png");
			break;
		case 55:
			hPict_[i] = Image::Load("STBar55.png");
			break;
		case 56:
			hPict_[i] = Image::Load("STBar56.png");
			break;
		case 57:
			hPict_[i] = Image::Load("STBar57.png");
			break;
		case 58:
			hPict_[i] = Image::Load("STBar58.png");
			break;
		case 59:
			hPict_[i] = Image::Load("STBar59.png");
			break;
		case 60:
			hPict_[i] = Image::Load("STBar60.png");
			break;
		case 61:
			hPict_[i] = Image::Load("STBar61.png");
			break;
		case 62:
			hPict_[i] = Image::Load("STBar62.png");
			break;
		case 63:
			hPict_[i] = Image::Load("STBar63.png");
			break;
		case 64:
			hPict_[i] = Image::Load("STBar64.png");
			break;
		case 65:
			hPict_[i] = Image::Load("STBar65.png");
			break;
		case 66:
			hPict_[i] = Image::Load("STBar66.png");
			break;
		case 67:
			hPict_[i] = Image::Load("STBar67.png");
			break;
		case 68:
			hPict_[i] = Image::Load("STBar68.png");
			break;
		case 69:
			hPict_[i] = Image::Load("STBar69.png");
			break;
		case 70:
			hPict_[i] = Image::Load("STBar70.png");
			break;
		case 71:
			hPict_[i] = Image::Load("STBar71.png");
			break;
		case 72:
			hPict_[i] = Image::Load("STBar72.png");
			break;
		case 73:
			hPict_[i] = Image::Load("STBar73.png");
			break;
		case 74:
			hPict_[i] = Image::Load("STBar74.png");
			break;
		case 75:
			hPict_[i] = Image::Load("STBar75.png");
			break;
		case 76:
			hPict_[i] = Image::Load("STBar76.png");
			break;
		case 77:
			hPict_[i] = Image::Load("STBar77.png");
			break;
		case 78:
			hPict_[i] = Image::Load("STBar78.png");
			break;
		case 79:
			hPict_[i] = Image::Load("STBar79.png");
			break;
		case 80:
			hPict_[i] = Image::Load("STBar80.png");
			break;
		case 81:
			hPict_[i] = Image::Load("STBar81.png");
			break;
		case 82:
			hPict_[i] = Image::Load("STBar82.png");
			break;
		case 83:
			hPict_[i] = Image::Load("STBar83.png");
			break;
		case 84:
			hPict_[i] = Image::Load("STBar84.png");
			break;
		case 85:
			hPict_[i] = Image::Load("STBar85.png");
			break;
		case 86:
			hPict_[i] = Image::Load("STBar86.png");
			break;
		case 87:
			hPict_[i] = Image::Load("STBar87.png");
			break;
		case 88:
			hPict_[i] = Image::Load("STBar88.png");
			break;
		case 89:
			hPict_[i] = Image::Load("STBar89.png");
			break;
		case 90:
			hPict_[i] = Image::Load("STBar90.png");
			break;
		case 91:
			hPict_[i] = Image::Load("STBar91.png");
			break;
		case 92:
			hPict_[i] = Image::Load("STBar92.png");
			break;
		case 93:
			hPict_[i] = Image::Load("STBar93.png");
			break;
		case 94:
			hPict_[i] = Image::Load("STBar94.png");
			break;
		case 95:
			hPict_[i] = Image::Load("STBar95.png");
			break;
		case 96:
			hPict_[i] = Image::Load("STBar96.png");
			break;
		case 97:
			hPict_[i] = Image::Load("STBar97.png");
			break;
		case 98:
			hPict_[i] = Image::Load("STBar98.png");
			break;
		case 99:
			hPict_[i] = Image::Load("STBar99.png");
			break;
		case 100:
			hPict_[i] = Image::Load("STBar100.png");
			
		}
	}

	//一番下に配置する
	transform_.position_.vecX -= 0.8f;
	transform_.position_.vecY -= 0.9f;

	transform_.scale_.vecX = 1.5f;
}

//更新
void STBar::Update()
{
	Player* pPlayer_ = (Player*)FindObject("Player");

	STflag = pPlayer_->ST_PL;


}




//描画
void STBar::Draw()
{
	Image::SetTransform(hPict_[STflag], transform_);
	Image::Draw(hPict_[STflag]);
}

//開放
void STBar::Release()
{
}













