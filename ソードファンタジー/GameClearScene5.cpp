#include "GameClearScene5.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Engine/Audio.h"

//コンストラクタ
GameClearScene5::GameClearScene5(GameObject* parent)
	: GameObject(parent, "GameClearScene5"), hPict_(-1)
{
}

//初期化
void GameClearScene5::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameClear5.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 3.0f;
	transform_.scale_.vecY = 3.0f;
}

//更新
void GameClearScene5::Update()
{
	//エンターキーが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//タイトルシーンに行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

	//スペースキーが押されていたら
	if (Input::IsKey(DIK_SPACE))
	{
		//プレイシーン6に行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY6);
	}
}

//描画
void GameClearScene5::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameClearScene5::Release()
{
}