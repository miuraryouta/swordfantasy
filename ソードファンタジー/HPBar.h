#pragma once
#include "Engine/GameObject.h"

//HPバーを管理するクラス
class HPBar : public GameObject
{
	int hPict_[101];    //画像番号

	int hSound_;     //サウンド番号

	int HPflag = 100;

public:



	//コンストラクタ
	HPBar(GameObject* parent);

	//デストラクタ
	~HPBar();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};