#include "SPBar.h"
#include "Engine/Image.h"
#include "Player.h"
#include "Engine/Audio.h"



//コンストラクタ
SPBar::SPBar(GameObject* parent)
	:GameObject(parent, "SPBar"), hSound_(-1)
{
	for (int i = 0; i < 101; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
SPBar::~SPBar()
{
}

//初期化
void SPBar::Initialize()
{

	//画像データのロード
//	hPict_ = Image::Load("SPBar.png");
//	assert(hPict_ >= 0);

	for (int i = 0; i < 101; i++)
	{
		switch (i) 
		{
			
		case 0:
			hPict_[i] = Image::Load("SPBar0.png");
			break;
		case 1:
			hPict_[i] = Image::Load("SPBar1.png");
			break;
		case 2:
			hPict_[i] = Image::Load("SPBar2.png");
			break;
		case 3:
			hPict_[i] = Image::Load("SPBar3.png");
			break;
		case 4:
			hPict_[i] = Image::Load("SPBar4.png");
			break;
		case 5:
			hPict_[i] = Image::Load("SPBar5.png");
			break;
		case 6:
			hPict_[i] = Image::Load("SPBar6.png");
			break;
		case 7:
			hPict_[i] = Image::Load("SPBar7.png");
			break;
		case 8:
			hPict_[i] = Image::Load("SPBar8.png");
			break;
		case 9:
			hPict_[i] = Image::Load("SPBar9.png");
			break;
		case 10:
			hPict_[i] = Image::Load("SPBar10.png");
			break;
		case 11:
			hPict_[i] = Image::Load("SPBar11.png");
			break;
		case 12:
			hPict_[i] = Image::Load("SPBar12.png");
			break;
		case 13:
			hPict_[i] = Image::Load("SPBar13.png");
			break;
		case 14:
			hPict_[i] = Image::Load("SPBar14.png");
			break;
		case 15:
			hPict_[i] = Image::Load("SPBar15.png");
			break;
		case 16:
			hPict_[i] = Image::Load("SPBar16.png");
			break;
		case 17:
			hPict_[i] = Image::Load("SPBar17.png");
			break;
		case 18:
			hPict_[i] = Image::Load("SPBar18.png");
			break;
		case 19:
			hPict_[i] = Image::Load("SPBar19.png");
			break;
		case 20:
			hPict_[i] = Image::Load("SPBar20.png");
			break;
		case 21:
			hPict_[i] = Image::Load("SPBar21.png");
			break;
		case 22:
			hPict_[i] = Image::Load("SPBar22.png");
			break;
		case 23:
			hPict_[i] = Image::Load("SPBar23.png");
			break;
		case 24:
			hPict_[i] = Image::Load("SPBar24.png");
			break;
		case 25:
			hPict_[i] = Image::Load("SPBar25.png");
			break;
		case 26:
			hPict_[i] = Image::Load("SPBar26.png");
			break;
		case 27:
			hPict_[i] = Image::Load("SPBar27.png");
			break;
		case 28:
			hPict_[i] = Image::Load("SPBar28.png");
			break;
		case 29:
			hPict_[i] = Image::Load("SPBar29.png");
			break;
		case 30:
			hPict_[i] = Image::Load("SPBar30.png");
			break;
		case 31:
			hPict_[i] = Image::Load("SPBar31.png");
			break;
		case 32:
			hPict_[i] = Image::Load("SPBar32.png");
			break;
		case 33:
			hPict_[i] = Image::Load("SPBar33.png");
			break;
		case 34:
			hPict_[i] = Image::Load("SPBar34.png");
			break;
		case 35:
			hPict_[i] = Image::Load("SPBar35.png");
			break;
		case 36:
			hPict_[i] = Image::Load("SPBar36.png");
			break;
		case 37:
			hPict_[i] = Image::Load("SPBar37.png");
			break;
		case 38:
			hPict_[i] = Image::Load("SPBar38.png");
			break;
		case 39:
			hPict_[i] = Image::Load("SPBar39.png");
			break;
		case 40:
			hPict_[i] = Image::Load("SPBar40.png");
			break;
		case 41:
			hPict_[i] = Image::Load("SPBar41.png");
			break;
		case 42:
			hPict_[i] = Image::Load("SPBar42.png");
			break;
		case 43:
			hPict_[i] = Image::Load("SPBar43.png");
			break;
		case 44:
			hPict_[i] = Image::Load("SPBar44.png");
			break;
		case 45:
			hPict_[i] = Image::Load("SPBar45.png");
			break;
		case 46:
			hPict_[i] = Image::Load("SPBar46.png");
			break;
		case 47:
			hPict_[i] = Image::Load("SPBar47.png");
			break;
		case 48:
			hPict_[i] = Image::Load("SPBar48.png");
			break;
		case 49:
			hPict_[i] = Image::Load("SPBar49.png");
			break;
		case 50:
			hPict_[i] = Image::Load("SPBar50.png");
			break;
		case 51:
			hPict_[i] = Image::Load("SPBar51.png");
			break;
		case 52:
			hPict_[i] = Image::Load("SPBar52.png");
			break;
		case 53:
			hPict_[i] = Image::Load("SPBar53.png");
			break;
		case 54:
			hPict_[i] = Image::Load("SPBar54.png");
			break;
		case 55:
			hPict_[i] = Image::Load("SPBar55.png");
			break;
		case 56:
			hPict_[i] = Image::Load("SPBar56.png");
			break;
		case 57:
			hPict_[i] = Image::Load("SPBar57.png");
			break;
		case 58:
			hPict_[i] = Image::Load("SPBar58.png");
			break;
		case 59:
			hPict_[i] = Image::Load("SPBar59.png");
			break;
		case 60:
			hPict_[i] = Image::Load("SPBar60.png");
			break;
		case 61:
			hPict_[i] = Image::Load("SPBar61.png");
			break;
		case 62:
			hPict_[i] = Image::Load("SPBar62.png");
			break;
		case 63:
			hPict_[i] = Image::Load("SPBar63.png");
			break;
		case 64:
			hPict_[i] = Image::Load("SPBar64.png");
			break;
		case 65:
			hPict_[i] = Image::Load("SPBar65.png");
			break;
		case 66:
			hPict_[i] = Image::Load("SPBar66.png");
			break;
		case 67:
			hPict_[i] = Image::Load("SPBar67.png");
			break;
		case 68:
			hPict_[i] = Image::Load("SPBar68.png");
			break;
		case 69:
			hPict_[i] = Image::Load("SPBar69.png");
			break;
		case 70:
			hPict_[i] = Image::Load("SPBar70.png");
			break;
		case 71:
			hPict_[i] = Image::Load("SPBar71.png");
			break;
		case 72:
			hPict_[i] = Image::Load("SPBar72.png");
			break;
		case 73:
			hPict_[i] = Image::Load("SPBar73.png");
			break;
		case 74:
			hPict_[i] = Image::Load("SPBar74.png");
			break;
		case 75:
			hPict_[i] = Image::Load("SPBar75.png");
			break;
		case 76:
			hPict_[i] = Image::Load("SPBar76.png");
			break;
		case 77:
			hPict_[i] = Image::Load("SPBar77.png");
			break;
		case 78:
			hPict_[i] = Image::Load("SPBar78.png");
			break;
		case 79:
			hPict_[i] = Image::Load("SPBar79.png");
			break;
		case 80:
			hPict_[i] = Image::Load("SPBar80.png");
			break;
		case 81:
			hPict_[i] = Image::Load("SPBar81.png");
			break;
		case 82:
			hPict_[i] = Image::Load("SPBar82.png");
			break;
		case 83:
			hPict_[i] = Image::Load("SPBar83.png");
			break;
		case 84:
			hPict_[i] = Image::Load("SPBar84.png");
			break;
		case 85:
			hPict_[i] = Image::Load("SPBar85.png");
			break;
		case 86:
			hPict_[i] = Image::Load("SPBar86.png");
			break;
		case 87:
			hPict_[i] = Image::Load("SPBar87.png");
			break;
		case 88:
			hPict_[i] = Image::Load("SPBar88.png");
			break;
		case 89:
			hPict_[i] = Image::Load("SPBar89.png");
			break;
		case 90:
			hPict_[i] = Image::Load("SPBar90.png");
			break;
		case 91:
			hPict_[i] = Image::Load("SPBar91.png");
			break;
		case 92:
			hPict_[i] = Image::Load("SPBar92.png");
			break;
		case 93:
			hPict_[i] = Image::Load("SPBar93.png");
			break;
		case 94:
			hPict_[i] = Image::Load("SPBar94.png");
			break;
		case 95:
			hPict_[i] = Image::Load("SPBar95.png");
			break;
		case 96:
			hPict_[i] = Image::Load("SPBar96.png");
			break;
		case 97:
			hPict_[i] = Image::Load("SPBar97.png");
			break;
		case 98:
			hPict_[i] = Image::Load("SPBar98.png");
			break;
		case 99:
			hPict_[i] = Image::Load("SPBar99.png");
			break;
		case 100:
			hPict_[i] = Image::Load("SPBar100.png");
			
		}
	}

	//サウンドデータをロード
	//hSound_ = Audio::Load("");
    //assert(hSound_ >= 0);

	//3番目の位置に配置する
	transform_.position_.vecX -= 0.8f;
	transform_.position_.vecY -= 0.95f;

	transform_.scale_.vecX = 1.5f;
}

//更新
void SPBar::Update()
{
	Player* pPlayer_ = (Player*)FindObject("Player");

	SPflag = pPlayer_->SP_PL;

	if (pPlayer_->flag1 == 1)
	{
		Audio::Play(hSound_);
	}
}




//描画
void SPBar::Draw()
{
	Image::SetTransform(hPict_[SPflag], transform_);
	Image::Draw(hPict_[SPflag]);
}

//開放
void SPBar::Release()
{
}













