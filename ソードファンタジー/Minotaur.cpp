#include "Minotaur.h"
#include "Sword.h"
#include "Player.h"
#include "FireMagic.h"
#include "Engine/Model.h"
#include "Engine/SceneManager.h"
#include "Engine/BoxCollider.h"
#include "Engine/Input.h"
#include "Ground.h"







//コンストラクタ
Minotaur::Minotaur(GameObject* parent)
	:GameObject(parent, "Minotaur"), hModel_(-1)
{
}

//デストラクタ
Minotaur::~Minotaur()
{
}

//初期化
void Minotaur::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Minotaur.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = 25;

	transform_.scale_.vecX = 2.5f;
	transform_.scale_.vecY = 2.5f;
	transform_.scale_.vecZ = 2.5f;

	transform_.rotate_.vecY = 180;

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 1.5, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	time = 5;

	time2 = 1;

	time3 = 10;

	Model::SetAnimFrame(hModel_, 1, 150, 2);


}

//更新
void Minotaur::Update()
{
	Move();       //移動処理

	Special();      //移動処理以外

}

void Minotaur::Move()
{
	Player* pPlayer;
	pPlayer = (Player*)FindObject("Player");
	XMVECTOR direction = { 0.0f, 0.0f, 1.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	direction = XMVector3TransformCoord(direction, mat);

	XMVECTOR distance = pPlayer->GetPosition() - transform_.position_;



	distance = XMVector3Normalize(distance);
	direction = XMVector3Normalize(direction);

	float naiseki = XMVector3Dot(direction, distance).vecX;
	float angle = acos(naiseki);


	angle = XMConvertToDegrees(angle);



	XMVECTOR move_mi = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_mi;
	mat_mi = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_mi = XMVector3TransformCoord(move_mi, mat_mi);



	if (VISION_MI >= angle)
	{
		//プレイヤーのいる方向に向かってゆく
		transform_.rotate_.vecY += 0;

		transform_.position_ += move_mi;
	}
	else if(VISION_MI > angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY += 2.0f;
	}
	else if (VISION_MI < angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY -= 2.0f;
	}
}

void Minotaur::Special()
{

	//HPが0以下になったとき
	if (HP_MI < 1)
	{
		KillMe(); //ミノタウロスが死ぬ

		//シーンが変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR5);

	}


	XMVECTOR move_mi = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_mi;
	mat_mi = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_mi = XMVector3TransformCoord(move_mi, mat_mi);

	if (flag == 1)
	{
		AT_MI = 0;

		transform_.position_ -= move_mi;

		//攻撃した後5秒間攻撃しない
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_MI)
			{


				switch (time)
				{
				case 5:TimerFlag_MI = 5; break;
				case 4:TimerFlag_MI = 4; break;
				case 3:TimerFlag_MI = 3; break;
				case 2:TimerFlag_MI = 2; break;
				case 1:TimerFlag_MI = 1; break;
				case 0:TimerFlag_MI = 0;
				}
				time--;
				TimerFlag_MI--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_MI == 0)
		{
			AT_MI = 20;
			flag = 0;
			time = 5;
			TimerFlag_MI = 5;
		}
	}


	//特殊攻撃
	if (flag3 == 0)
	{
		Rand_MI = rand() % 10;
		
		flag3 = 1;
	}
	else if (flag3 == 1)
	{
		if (Rand_MI == 1)
		{
			AT_MI += 5;

			if (flag4 == 0)
			{
				move_mi = move_mi * 2;
			}
			else if (flag4 == 1)
			{
				move_mi = move_mi / 2;
				flag4 = 0;
			}

			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_MI3)
				{
					switch (time3)
					{
					case 10:TimerFlag_MI3 = 10; break;
					case 9:TimerFlag_MI3 = 9; break;
					case 8:TimerFlag_MI3 = 8; break;
					case 7:TimerFlag_MI3 = 7; break;
					case 6:TimerFlag_MI3 = 6; break;
					case 5:TimerFlag_MI3 = 5; break;
					case 4:TimerFlag_MI3 = 4; break;
					case 3:TimerFlag_MI3 = 3; break;
					case 2:TimerFlag_MI3 = 2; break;
					case 1:TimerFlag_MI3 = 1; break;
					case 0:TimerFlag_MI3 = 0;
					}
					time3--;
					TimerFlag_MI3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_MI3 == 0)
			{
				Rand_MI = 0;
				AT_MI -= 5;
				flag3 = 0;
				time3 = 10;
				TimerFlag_MI3 = 10;
			}
		}
		else if (Rand_MI > 1)
		{
			//10秒間待つ
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time3 == TimerFlag_MI3)
				{
					switch (time3)
					{
					case 10:TimerFlag_MI3 = 10; break;
					case 9:TimerFlag_MI3 = 9; break;
					case 8:TimerFlag_MI3 = 8; break;
					case 7:TimerFlag_MI3 = 7; break;
					case 6:TimerFlag_MI3 = 6; break;
					case 5:TimerFlag_MI3 = 5; break;
					case 4:TimerFlag_MI3 = 4; break;
					case 3:TimerFlag_MI3 = 3; break;
					case 2:TimerFlag_MI3 = 2; break;
					case 1:TimerFlag_MI3 = 1; break;
					case 0:TimerFlag_MI3 = 0;
					}
					time3--;
					TimerFlag_MI3--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_MI3 == 0)
			{
				Rand_MI = 0;
				flag3 = 0;
				time3 = 10;
				TimerFlag_MI3 = 10;
			}
		}
	}
}

//描画
void Minotaur::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Minotaur::Release()
{
}

void Minotaur::OnCollision(GameObject * pTarget)
{
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");
	Player* pPlayer_ = (Player*)FindObject("Player");
	Ground* pGround_ = (Ground*)FindObject("Ground");


	//剣に当たったとき
	if (pTarget->GetObjectName() == "Sword")
	{
		//Eキーを押した
		if (Input::IsKeyDown(DIK_E))
		{
			if (pPlayer_->flag7 != 5)
			{


				pPlayer_->MP_PL += 5;

				if (pPlayer_->flag1 == 0)
				{
					pPlayer_->SP_PL += 5;
				}

				//HPが減る
				HP_MI = HP_MI - pSword_->AT_S;

				
				pPlayer_->flag7 += 1;
				
			}
		}



	}

	//魔法に当たったとき
	if (pTarget->GetObjectName() == "FireMagic")
	{
		HP_MI = HP_MI - pFireMagic_->AT_FM;
		pTarget->KillMe(); //魔法が消える
	}

	XMVECTOR move_mi = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_mi;
	mat_mi = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_mi = XMVector3TransformCoord(move_mi, mat_mi);

	//プレイヤーにあったとき
	if (pTarget->GetObjectName() == "Player")
	{


		transform_.position_ -= move_mi;

		if (flag2 == 0)
		{
			flag2 = 1;
		}
		else if (flag2 == 1)
		{
			AT_MI = 0;

			

			//攻撃前1秒間攻撃しない
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time2 == TimerFlag_MI2)
				{


					switch (time2)
					{
					case 1:TimerFlag_MI2 = 1; break;
					case 0:TimerFlag_MI2 = 0;
					}
					time2--;
					TimerFlag_MI2--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_MI2 == 0)
			{
				AT_MI = 20;
				flag2 = 0;
				time2 = 1;
				TimerFlag_MI2 = 1;
			}
		}

		if (flag == 0)
		{
			pPlayer_->HP_PL = pPlayer_->HP_PL - AT_MI;
			flag = 1;
		}

		if (flag4 == 0)
		{
			flag4 = 1;
		}


	}

	//に当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		transform_.position_ -= move_mi;
	}
}

void Minotaur::CountDown()
{
	time -= 1;

	time2 -= 1;

	time3 -= 1;
}
