#include "Sword.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"


//コンストラクタ
Sword::Sword(GameObject * parent)
	:GameObject(parent, "Sword"), hModel_(-1), hSound_(-1)
{
}

//デストラクタ
Sword::~Sword()
{
}

//初期化
void Sword::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Sword2.fbx");
	assert(hModel_ >= 0);



	//初期位置
	transform_.position_.vecX = 0.7;
	transform_.position_.vecY = 0.5;
	transform_.position_.vecZ = 0.5;

	//transform_.rotate_.vecY = 90;

	transform_.scale_.vecX = 0.1;
	transform_.scale_.vecY = 0.1;
	transform_.scale_.vecZ = 0.1;


	//当たり判定が出る
	BoxCollider* collision = new BoxCollider(XMVectorSet(-0.7, 1, -0.5, 0), XMVectorSet(3.5, 2, 3.5, 0));
	AddCollider(collision);

	//サウンドをロードするとエラーが起こる
	//サウンドデータのロード
//	hSound_ = Audio::Load("Sword.wav");
//	assert(hSound_ >= 0);

	Model::SetAnimFrame(hModel_, 1, 150, 2);
	
}

//更新
void Sword::Update()
{


	//Eキーを押した
	if (Input::IsKeyDown(DIK_E))
	{
//		Audio::Play(hSound_);
	}

	
}

//描画
void Sword::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Sword::Release()
{
}




