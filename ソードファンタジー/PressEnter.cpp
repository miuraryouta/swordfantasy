#include "PressEnter.h"
#include "Engine/Image.h"



//コンストラクタ
PressEnter::PressEnter(GameObject* parent)
	:GameObject(parent, "PressEnter"), hPict_(-1)
{
}

//デストラクタ
PressEnter::~PressEnter()
{
}

//初期化
void PressEnter::Initialize()
{

	//画像データのロード
	hPict_ = Image::Load("PressEnter.png");
	assert(hPict_ >= 0);

	transform_.position_.vecY -= 0.5;

	time_PE = 1;

	
}

//更新
void PressEnter::Update()
{


		//解決
		if (flag_PE == 0)
		{


			if (flag_PE2 == 1)
			{
				transform_.scale_ += PE1;
			}
			else if (flag_PE3 == 1)
			{
				transform_.scale_ -= PE1;
			}

			//1秒間拡大する
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time_PE == TimerFlag_PE)
				{


					switch (time_PE)
					{
					case 1:TimerFlag_PE = 1; break;
					case 0:TimerFlag_PE = 0;
					}
					time_PE--;
					TimerFlag_PE--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_PE == 0)
			{
				flag_PE = 0;
				time_PE = 1;
				TimerFlag_PE = 1;

				if (flag_PE2 == 0)
				{
					flag_PE2 = 1;
				}
				else if (flag_PE2 == 1)
				{
					flag_PE2 = 0;
				}

				if (flag_PE3 == 0)
				{
					flag_PE3 = 1;
				}
				else if (flag_PE3 == 1)
				{
					flag_PE3 = 0;
				}
			}
		}

}




//描画
void PressEnter::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void PressEnter::Release()
{
}

void PressEnter::CountDown()
{
	time_PE -= 1;

	
}











