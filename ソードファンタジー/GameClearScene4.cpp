#include "GameClearScene4.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Engine/Audio.h"

//コンストラクタ
GameClearScene4::GameClearScene4(GameObject* parent)
	: GameObject(parent, "GameClearScene4"), hPict_(-1)
{
}

//初期化
void GameClearScene4::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameClear4.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 3.0f;
	transform_.scale_.vecY = 3.0f;
}

//更新
void GameClearScene4::Update()
{
	//エンターキーが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//タイトルシーンに行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

	//スペースキーが押されていたら
	if (Input::IsKey(DIK_SPACE))
	{
		//プレイシーン5に行く
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY5);
	}
}

//描画
void GameClearScene4::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameClearScene4::Release()
{
}