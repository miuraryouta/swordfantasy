#include "PlayScene6.h"
#include "Player.h"
#include "Ground.h"
#include "Dragon.h"
#include "HPBar.h"
#include "MPBar.h"
#include "SPBar.h"
#include "STBar.h"
#include "Background.h"
#include "Engine/Audio.h"
#include "Engine/Image.h"

//コンストラクタ
PlayScene6::PlayScene6(GameObject * parent)
	: GameObject(parent, "PlayScene6"), hPict_(-1)
{
}

//初期化
void PlayScene6::Initialize()
{
	Instantiate<Player>(this);
	Instantiate<Ground>(this);
	Instantiate<Dragon>(this);
	Instantiate<HPBar>(this);
	Instantiate<MPBar>(this);
	Instantiate<SPBar>(this);
	Instantiate<STBar>(this);
	Instantiate<Background>(this);

	//画像データのロード
	//hPict_ = Image::Load("");
	//assert(hPict_ >= 0);

	
}

//更新
void PlayScene6::Update()
{
}

//描画
void PlayScene6::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}


//開放
void PlayScene6::Release()
{
}